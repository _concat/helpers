<?php

namespace Concat\Helpers\Strings;

function array_flatten(array $array)
{
    $result = [];
    array_walk_recursive($array, function ($x) use (&$result) {
        $result[] = $x;
    });

    return $result;
}
