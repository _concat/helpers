<?php

namespace Concat\Helpers\Arrays;

require_once 'Arrays/Combinations.php';

function combinations($items, $count = null)
{
    $iterator = new Combinations($items, $count);
    foreach ($iterator as $combination) {
        yield $combination;
    }
}

function array_flatten(array $array)
{
    $result = [];
    array_walk_recursive($array, function ($x) use (&$result) {
        $result[] = $x;
    });

    return $result;
}

function array_dot(array $array)
{
    $dot_array = [];

    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($array));

    foreach ($iterator as $leaf) {
        $keys = [];

        $depth = $iterator->getDepth();

        for ($level = 0; $level <= $depth; $level++) {
            $keys[] = $iterator->getSubIterator($level)->key();
        }

        //
        $dot_array[join('.', $keys)] = $leaf;
    }

    return $dot_array;
}
