<?php

namespace Concat\Helpers\Paths;

function join_paths(...$levels)
{
    switch (count($levels)) {
        case 0:
            return "";
        case 1:
            return $levels[0];
        default:
            $absolute = $levels[0][0] === "/";

            $path = join("/", array_map(function ($level) {
                return trim($level, "/");
            }, $levels));

            if ($absolute) {
                $path = "/".$path;
            }

            return $path;
    }
}

function check_extension($path, $extension)
{
    if ($extension && pathinfo($path, PATHINFO_EXTENSION) !== $extension) {
        $path = "$path.$extension";
    }

    return $path;
}

// function normalize_path($path)
// {
//     if(!$path){
//         return ".";
//     }

//     $absolute = $path[0] == "/";

//     $path = join_paths(...explode("/", $path));

//     if($absolute){
//         $path = "/" . $path;
//     }

//     return $path;
// }
