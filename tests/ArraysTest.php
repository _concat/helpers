<?php

namespace Concat\Helpers\Tests;

use function \Concat\Helpers\Arrays\array_flatten;

class ArraysTest extends \PHPUnit_Framework_TestCase
{
    public function test_flatten_nested()
    {
        // flatten a nested array
        $s = [1, 2, [3, 4, [5, 6]]];
        $e = [1, 2, 3, 4, 5, 6];
        $a = array_flatten($s);
        $this->assertEquals($e, $a);
    }

    public function test_flatten_already_flat()
    {
        // flatten a nested array
        $s = [1, 2, 3];
        $e = [1, 2, 3];
        $a = array_flatten($s);
        $this->assertEquals($e, $a);
    }

    public function test_flatten_empty_array()
    {
        // flatten a nested array
        $s = [];
        $e = [];
        $a = array_flatten($s);
        $this->assertEquals($e, $a);
    }

    public function test_flatten_nested_empty_arrays()
    {
        // flatten a nested array
        $s = [[], [], [[], []]];
        $e = [];
        $a = array_flatten($s);
        $this->assertEquals($e, $a);
    }
}
