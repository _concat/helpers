<?php

namespace Concat\Helpers\Tests;

use function \Concat\Helpers\Paths\join_paths;

class PathsTest extends \PHPUnit_Framework_TestCase
{
    public function testJoin()
    {
        // flatten a nested array
        $s = ['/root', '/relative/', 'file'];
        $e = "/root/relative/file";
        $a = join_paths(...$s);
        $this->assertEquals($e, $a);
    }
}
